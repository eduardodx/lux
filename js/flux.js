var pallets = [
    /* Indecent Proposal */
    ['#6F5846', '#A95A52', '#E35B5D', '#F18052', '#FFA446', '#6F5846'],
    /* Invisible look! */
    ['#029DAF', '#E5D599', '#FFC219', '#F07C19', '#E32551', '#029DAF']
];

var palletChosen = 1;
var baseTime = 5000;

/*****************************************************/

var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;

var context = document.getElementsByTagName('canvas')[0].getContext('2d');
var rekapi = new Rekapi(context);

rekapi.renderer.height(y);
rekapi.renderer.width(x);

var actor = new Rekapi.Actor({
    // Draws a circle
    'render': function(ctx, state) {
        ctx.fillStyle = state.color;
        context.fillRect(0, 0, x, y);
    }
});

rekapi.addActor(actor);

for (var i = 0; i < pallets[palletChosen].length; ++i) {
    actor.keyframe(i * baseTime, {
        color: pallets[palletChosen][i]
    });
}

rekapi.play();
